package com.miracle.sp;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;


@Component
public class EmployeeDao {


	private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

	public List<Employee> list() {
		Session session = this.sessionFactory.openSession();
		List<Employee> personList = session.createQuery("from Employee").list();
		session.close();
		return personList;
	}
}
