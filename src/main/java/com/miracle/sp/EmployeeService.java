package com.miracle.sp;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.miracle.sp.EmployeeDao;

@Service
public class EmployeeService {


	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");

	public List<Employee> list(){
		
		EmployeeDao emplDAO = context.getBean(EmployeeDao.class);

		List<Employee> list = emplDAO.list();
		
		return list;
		
	}

}
